/*
Grunt tasks

Copyright 2015 Oliver Gutierrez <ogutsua@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

module.exports = function (grunt) {

    // Tasks configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        compass: {
            dist: {
                options: {
                    sassDir: 'src/sass',
                    cssDir: 'themes/<%= pkg.name %>/static/css',
                    imagesDir: 'src/img',
                    generatedImagesDir: 'themes/<%= pkg.name %>/static/img',
                    httpGeneratedImagesPath: '../img',
                    javascriptsDir: 'themes/<%= pkg.name %>/static/js',
                    outputStyle: 'compressed',
                    noLineComments: true,
                    trace: true,
                    environment: 'production'
                }
            }
        },

        concat: {
            options: {
                separator: ';',
                process: false
            },
            dist: {
                src: ['src/js/*.js'],
                dest: 'themes/<%= pkg.name %>/static/js/scripts.js'
            }
        },

        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyymmddhhmmss") %> */\n'
            },
            dist: {
                files: {
                    '<%= concat.dist.dest %>': ['<%= concat.dist.dest %>']
                }
            }
        },

        jslint: {
            frontend: {
                src: [
                    'src/js/*.js'
                ],
                exclude: [
                ],
                directives: {
                    browser: true,
                    unparam: true,
                    todo: true,
                    predef: [
                        'jQuery',
                        '$',
                        'console'
                    ]
                },
                options: {
                    errorsOnly: true,
                    failOnError: false,
                    shebang: true,
                    tabSize: 2
                }
            }
        },

        copy: {
            images: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/img/',
                        src: ['**/*.{png,jpg,svg}'],
                        dest: 'themes/<%= pkg.name %>/static/img/'
                    }
                ]
            },
            fonts: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/fonts/',
                        src: ['**/*'],
                        dest: 'themes/<%= pkg.name %>/static/fonts/'
                    }
                ]
            }
        },

        watch: {
            stylesheets: {
                files: ['src/sass/**/*.scss', 'src/sass/**/*.sass', 'src/img/**/*'],
                tasks: ['compass'],
            },
            scripts: {
                files: ['src/js/**/*.js'],
                tasks: ['jslint', 'concat'],
            },
            images: {
                files: ['src/img/**/*'],
                tasks: ['copy']
            }
        }
    });

    // Plugin load
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Tasks
    grunt.registerTask('devel', ['watch']);
    grunt.registerTask('default', ['compass', 'concat', 'uglify', 'copy']);
};
