## GUADEC pelican website generator

This is a static website generator based in Pelican


### Dependencies

For using this website generator you need to have in your system a working python install with pip, compass ruby gem and node.js. They are usually available in any distro.

### Initial setup

The only thing you need to start is to execute

    ./devel.sh

This will install all the python packages you need into a virtual environment
and the node packages needed for grunt tasks to run.

Once the command finishes the execution you will have a development webserver running at

    http://localhost:8000


### Writing content

Under the content folder you can add content for the website. Articles and pages are written in [Markdown](https://daringfireball.net/projects/markdown/syntax).

For more information about writing content for pelican, you can have a look at this [documentation](http://docs.getpelican.com/en/stable/content.html).

#### Articles

To add an article, just add a new markdown file under **content** directory. The file should at least have the following information:

    Title: Article title
    Date: 20180219
    Category: News

    Text for the article here

Article date must be in the format YYYYMMDD HH:MM:SS and the time is optional

#### Pages

The pages works the same way the articles do. The difference is that pages are not shown in any category and they live at **content/pages** directory.

#### Images and downloadable content

The images are placed ad **content/images** directory and can be included into documents by using:

    ![ALT Text]({filename}/images/image-name.jpg "Image title")


### Configuration options

There are some customizations made for GUADEC website. The way to use them is to edit the **pelicanconf.py** file.

It contains some variables with a very clear structure that can be customized for different purproses:

* **GUADEC_LOACATION**: Name of the location displayed at index page header
* **GUADEC_DATES**: Dates displayed at index page header
* **MENU**: Structure for the top menu
* **LINKS**: Links shown in the first column of the foot links
* **GUADEC_LINKS**: Links shown in the second column of the foot links
* **SOCIAL**: Social URLs to show at index page and at page foot.
* **SPONSORS**: List of sponsors that will be shown at index page
* **OPEN_REGISTRATION**: If True, enables registration links. Otherwise they are disabled.
