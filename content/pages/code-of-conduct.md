Title: Code of conduct
Date: 20180219

This text documents what you can expect from the GNOME community when you attend GUADEC and, in turn, what we expect from you.

This document has been agreed to by the local GUADEC organisation team.

GUADEC is a welcoming and friendly event, during which attendees often make friends and join the GNOME community. GUADEC organisers are dedicated to providing a safe and harassement-free conference experience for everyone. To help set expectations and standards for the event, we ask all attendees, organizers and sponsors to follow this code of conduct (CoC) at all conference venues and conference-related social events.

We ask you to be kind and respectful to others. Keep all communication appropriate for the audience, including people of different ages and different cultural backgrounds. GUADEC attendees come from all over the world, so something that you consider appropriate may not be appropriate to someone else. Do not insult or put down other attendees. Remember that sexist, racist or discriminatory language and imagery are not appropriate.

We believe that people generally mean well. Excepting strong evidence to the contrary, we ask that you do the same. We favour communication over confrontation: most issues that arise during a conference (as with day to day contribution to any software project) can be resolved by communication between the affected parties.

However, people do make mistakes. People are capable of engaging in thoughtless actions and are capable of making thoughtless remarks. Sometimes these actions and words cross a line. Even if you are not yourself offended by this behaviour, we ask that you discretely remind people that others may be. Be careful to not engage in actions that may escalate the situation.

If you observe or experience any behaviour that you consider to be offensive or harassing, you can talk to or file a formal complaint with any member of the support team. The support team will be available to help you in a number of ways. This includes, but is not limited to, listening to you, mediating between you and another attendee, and helping you with the complaint process. Any communication will remain confidential and be taken seriously. All reports will be investigated. We do not engage in public shaming of any kind and we ask the same of you.

The GUADEC organisers will take any actions that they consider appropriate for a report. This includes, but is not limited to:

* Ask the person concerned to stop or modify their behaviour
* Ask the person concerned to leave the venue or event immediately for the remainder of the day
* Ask the person concerned to leave immediately and prohibit them from returning to GUADEC
* Help you report the incident to the local authorities

#### Contacts:

* GUADEC Help Telephone: +44 773 365 7336
* Email: code-of-conduct@gnome.org
* Individual contacts: Emmanuele Bassi, Benjamin Berg, Rosanna Yuen

#### Other useful numbers/links:

* Emergency number for police, fire department, or ambulance: 999
* MMU Security emergency contact: +44 161 247 2222


## Incident response guidelines

Should you observe or experience behaviour that is disrupting to the friendly environment of the event then please get into contact with our support team about it. In some cases this may even make sense for seemingly minor incidents if the observed behaviour may become harassing through frequent repetition.

If an ongoing conflict is observed then please consult with the support team immediately and try to separate the involved parties if possible (if police intervention is in order do not hesitate to call them directly). The support team will try to reinstate the peace and investigate the incident afterwards by taking reports from everyone involved and observing third parties.

The support team and organisers shall assist any party who wants to press charges by notifying and guiding them to the relevant authorities. The support team should offer further assistance to all parties (e.g. by contacting a friend, arranging an escort) to help them deal with the aftermath of an incident.

Enforcement actions will usually only be taken be taken by the organisers once a written report is submitted by the support team. Any person at the receiving end of such action shall be given a chance to dispute the claims against them. Failure to cooperate with the organisers will result in immidate actions and may also result in further actions including criminal charges if applicable.

The GUADEC organisers commit to publish an anonymized summary about the nature of reported incidents and actions that were taken.


### Responding to incidents during Presentations

Presentations or similar events should not be interrupted unless there are major incidents that are of longer duration or happen repeatedly. If the presentation is already being interrupted due to an incident (e.g. medical) or if the GUADEC organizers, a support team member, or a volunteer decides to interrupt it in response to an incident, then the video streaming should be stopped or at least
the audio muted.

If you need to interrupt the presentation to respond to an incident, then remember to stay polite while doing so. In the case of a presentation being stopped entirely the presenter should be given a chance to quickly conclude it for the local audience and for streaming purposes if possible.

The GUADEC organisers may decide to not distribute recordings of presentations further because of incidents or for other reasons.


### Taking reports

Taking reports is important to understand what happened and a basis for deciding what further actions might be taken. Support team members shall take reports from all involved parties if possible. Do not rush when taking reports and ensure that you take the report in an environment that protects everyone’s privacy and is reassuring.

Please ensure that the report accurately describes what happened and clearly marks anything which has not been directly observed but is of a notional nature. If possible the report should include information about

the time and date of the incident,
the identity of involved parties,
possibly third parties who may have observed the incident,
a description of the observed behaviour,
the circumstances surrounding the incident including
actions that were done in response to it, and
notes on additional privacy constraints from the reporter.
Not all of this information will always be available and the reporter may decide to withhold any information without reason. Do not press the person for details but inform them that they can return at a later point. Even an incomplete report may be helpful.

The reporter may abort the process at any time. The report will be kept private and only handled by the organisers and support team members. The reporter may ask for the contained information not to be used as a basis for any further actions such as questioning an alleged offender and deciding on enforcement.


## Privacy statement

GUADEC will protect the privacy of all individuals at all points. Private information will not be disclosed to third parties with the only exception listed below.

All copies of reports shall be surrendered to the conference organisers and the reporting person by support team members. Support team members or GUADEC organisers shall not retain personal copies of reports or other private data connected to them. Private data must not be transferred to third party services (including the GNOME Foundation infrastructure). Documents related to reports
and incidents shall be destroyed once they are not needed anymore or at the latest two years after the conference.

The GUADEC organisers reserve the right to create a public report outlining the incidents that were reported. In some cases names of alleged offenders may be shared with third parties for future preventive measures (e.g. a warning to next years GUADEC organisers). GUADEC will not disclose detailed information from reports to any third party including the GNOME Foundation or law enforcement agencies unless required by law or authorized by the reporter.
