Title: Contact
Date: 20180219

GUADEC 2018 is organized by a Spain-based unincorporated association of volunteers, supported by the GNOME Foundation.

The lead organizer can be contacted at:


> Ismael Olea

> In tha house

> Telephone: +34 XXX XXX XXX

For conference issues, please get in touch on the GUADEC mailing list, or for sensitive issues use the private mailing list guadec-organization@gnome.org.
