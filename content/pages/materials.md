Title: Materials
Date: 20180219

Here we provide some materials to be used by attendees or speakers.


## Badges

You can use these badges for e.g. blogpost to promote the fact that you are going to GUADEC or are even speaking at the conference!

![GUADEC badge]({filename}/images/guadec-badge.png "GUADEC badge")

## Posters

We have a [poster](https://github.com/gnome-design-team/gnome-marketing/tree/master/events/guadec/2017/posters-and-banners) that can be used to promote GUADEC 2017.


## Slide Templates

We have slide templates for GUADEC available in the [presentation-templates repository](https://git.gnome.org/browse/presentation-templates). As of this writing (11 July), the template can be used with Markdown or LaTeX (via Pandoc). Any help with further formats is appreciated and can be added to the repository.
