#!/bin/sh

OUTPUT_DIR='output'
RSYNC_SERVER='root@matrixhas.me:/srv/nginx/websites'
SITE_DIRECTORY='guadec.matrixhas.me'

if [ ! -d env ]; then
    echo "Creating virtual environment"
    virtualenv -p python3 env
    echo "Activating virtual environment"
    source ./env/bin/activate
    echo "Installing python development dependencies"
    pip install -r requirements.txt
else
    echo "Activating virtual environment"
    source ./env/bin/activate
fi

echo "Removing old data"
rm -rf $OUTPUT_DIR

echo "Generating website in $OUTPUT_DIR directory"
pelican -s publishconf.py

echo "Syncing website content to $2"
rsync -avc -e "ssh -p 2022" --delete output/ $RSYNC_SERVER/$SITE_DIRECTORY/

echo "Deactivating virtual environment"
deactivate
