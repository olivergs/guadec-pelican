#/bin/bash

function quit {
    #echo "Killing Grunt watcher"
    #kill -9 $GRUNT_PID
    echo "Killing HAMLPy watcher"
    kill -9 $HAMLPY_PID
    echo "Killing Pelican watcher"
    kill -9 $PELICAN_PID
    echo "Deactivating virtual environment"
    deactivate
}

trap "quit" SIGINT

if [ ! -d env ]; then
    echo "Creating virtual environment"
    virtualenv -p python3 env
    echo "Activating virtual environment"
    source ./env/bin/activate
    echo "Installing python development dependencies"
    pip install -r requirements.txt
else
    echo "Activating virtual environment"
    source ./env/bin/activate
fi

if [ ! -d node_modules ]; then
    echo "Installing node.js modules"
    npm install
fi

echo "Executing grunt tasks and start watcher"
grunt &
grunt devel &
GRUNT_PID=$!

echo "Executing HAMLPy watcher"
hamlpy-watcher ./src/haml/ ./themes/website/templates/ --attr-wrapper \" &
HAMLPY_PID=$!

echo "Executing pelican processor with autoreload"
pelican -r &
PELICAN_PID=$!

echo "Executing pelican server"
mkdir -p output
cd output
python -m pelican.server
