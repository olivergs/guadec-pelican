#!/usr/bin/env python
# -*- coding: utf-8 -*- #
#
# Copyright 2015 Oliver Gutierrez <ogutsua@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from __future__ import unicode_literals

AUTHOR = 'GUADEC The GNOME Conference'
SITENAME = 'GUADEC 2018'
SITEYEAR = 2018
SITEDOMAIN = 'localhost'
SITEURL = 'http://%s:8000' % SITEDOMAIN

GUADEC_LOCATION = "Almería, Spain"
GUADEC_DATES = "July 6<sup>th</sup> - 11<sup>th</sup>"
OPEN_REGISTRATION = False

# Menu links
MENU = {
    'Home': '/',
    'Core Days': {
        'Venue': '/pages/venue.html',
        'Schedule': '/pages/schedule.html',
        '': None,
        'Social events': '/pages/social-events.html',
    },
    'Unconference': '/pages/unconference.html',
    'Travel': '/pages/travel.html',
    'Code of Conduct': '/pages/code-of-conduct.html',
    'Sponsors': '/pages/sponsors.html',
    'Materials': '/pages/materials.html',
    'Contact': '/pages/contact.html',
}

LINKS = (
    ('The GNOME Project', 'https://www.gnome.org/'),
    ('About Us', 'https://www.gnome.org/about/'),
    ('Get Involved', 'https://www.gnome.org/get-involved/'),
    ('Support GNOME', 'https://www.gnome.org/support-gnome/'),
    ('Merchandise', 'https://www.gnome.org/merchandise/'),
    ('Contact Us', 'https://www.gnome.org/contact/'),
    ('The GNOME Foundation', 'https://www.gnome.org/foundation/'),
)

# GUADEC specific links
GUADEC_LINKS = [
    ('GUADEC: The GNOME conference', '/'),
    ('News', '/category/news.html'),
    ('Travel', '/pages/travel.html'),
    ('Accomodation', '/pages/travel.html#accommodation'),
    ('Location', '/pages/travel.html#location'),
    ('Code of conduct', '/pages/code-of-conduct.html'),
    ('Sponsors', '/pages/sponsors.html'),
]

# Add registration link only if registration is open
if OPEN_REGISTRATION:
    GUADEC_LINKS.append(
        ('Register', 'https://registration.guadec.org/'))

# Social networks
SOCIAL = (
    ('Facebook', 'https://www.facebook.com/GNOMEDesktop'),
    ('Twitter', 'https://twitter.com/gnome'),
    ('Google Plus', 'https://plus.google.com/+gnome'),
    ('Youtube', 'https://www.youtube.com/user/GNOMEDesktop'),
    # ('Instagram', 'https://instagram.com/'),
    # ('Pinterest', 'https://pinterest.com/'),
)

SPONSORS = (
    # ('Private Internet Access', 'https://www.privateinternetaccess.com/', 'pia.png'),
    # ('Google', 'https://opensource.google.com/', 'google.png'),
    # ('Endless', 'https://endlessos.com/', 'endless.png'),
    # ('Red Hat', 'https://www.redhat.com/', 'redhat.png'),
    # ('OpenSUSE', 'http://www.opensuse.org/', 'opensuse.png'),
    # ('Igalia', 'http://www.igalia.com/', 'igalia.png'),
    # ('Mozilla Foundation', 'http://www.mozilla.org/', 'mozilla.png'),
    # ('Collabora', 'https://www.collabora.com/', 'collabora.png'),
)

#------------------------------------------------------------------------------
# Pelican settings
#------------------------------------------------------------------------------
PATH = 'content'
TIMEZONE = 'GMT'
DEFAULT_LANG = u'en'
THEME = 'themes/website'
DEFAULT_PAGINATION = False
DISPLAY_PAGES_ON_MENU = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
